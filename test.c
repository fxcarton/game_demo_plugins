#include <stdio.h>
#include <3dsh/shell.h>
#include <3dsh/builtins.h>

static int plugin_hello_world(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    fputs("Hello world from test plugin!\n", stdout);
    variable_make_int(42, ret);
    return 1;
}

int demo_plugin_init(struct DemoShell* shell, unsigned int apiver) {
    if (apiver != DEMO_API_VERSION) return 0;
    return builtin_function(shell, "plugin_hello_world", plugin_hello_world);
}
