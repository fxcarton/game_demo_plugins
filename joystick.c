#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/joystick.h>
#include <3dsh/builtins.h>
#include <3dsh/shell.h>
#include <3dsh/render.h>

struct Joystick {
    int axes[32];
    unsigned long btns;
    unsigned int numBtns, numAxes;
    int fd;
};

static void joystick_destroy(struct DemoShell* shell, void* d) {
    struct Joystick* joystick = d;
    close(joystick->fd);
    free(d);
}

static int joystick_copy(const struct Variable* src, struct Variable* dest) {
    void* data = src->data.dstruct.data;
    if (!shell_incref(data)) return 0;
    variable_make_struct(src->data.dstruct.info, data, dest);
    return 1;
}

static void joystick_free(void* data) {
    shell_decref(data);
}

static void joystick_process_event(const struct js_event* event, struct Joystick* joystick) {
    switch (event->type & ~JS_EVENT_INIT) {
        case 1:
            if (event->number >= joystick->numBtns) {
                joystick->numBtns = event->number + 1U;
            }
            if (event->value) {
                joystick->btns |= 1U << event->number;
            } else {
                joystick->btns &= ~(1U << event->number);
            }
            break;
        case 2:
            if (event->number >= sizeof(joystick->axes) / sizeof(*joystick->axes)) {
                break;
            }
            if (event->number >= joystick->numAxes) {
                joystick->numAxes = event->number + 1U;
            }
            joystick->axes[event->number] = event->value;
            break;
    }
}

static int array_get(const void* src, unsigned int i, struct Variable* dest) {
    const unsigned int* count = src;
    const float* values = (const void*)(count + 1);
    if (i >= *count) return 0;
    variable_make_float(values[i], dest);
    return 1;
}

static int array_add(void* dest, const struct Variable* src) {
    return 0;
}

static int array_del(void* dest, unsigned int i) {
    return 0;
}

static int array_copy(const struct Variable* src, struct Variable* dest) {
    float* values;
    unsigned int* count;
    size_t n = sizeof(*count) + ((size_t)(*src->data.darray.count )) * sizeof(*values);
    if (!(count = malloc(n))) return 0;
    memcpy(count, src->data.darray.data, n);
    variable_make_array(src->data.darray.info, count, count, dest);
    return 1;
}

static struct ArrayInfo arrayFloat = {array_get, array_add, array_del, array_copy, free};

static int joystick_get_axes(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    const struct Joystick* joystick = r->data;
    float* values;
    unsigned int* count;
    unsigned int i, n = joystick->numAxes;
    if (!(count = malloc(sizeof(*count) + ((size_t)n) * sizeof(*values)))) return 0;
    *count = n;
    values = (void*)(count + 1);
    for (i = 0; i < n; i++) values[i] = ((float)joystick->axes[i]) / 32767.0f;
    variable_make_array(&arrayFloat, count, count, dest);
    return 1;
}

static int joystick_get_buttons(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    const struct Joystick* joystick = r->data;
    float* values;
    unsigned int* count;
    unsigned int i, n = joystick->numBtns;
    if (!(count = malloc(sizeof(*count) + ((size_t)n) * sizeof(*values)))) return 0;
    *count = n;
    values = (void*)(count + 1);
    for (i = 0; i < n; i++) values[i] = !!(joystick->btns & (1U << i));
    variable_make_array(&arrayFloat, count, count, dest);
    return 1;
}

static struct FieldInfo structJoystickFields[] = {
    {"axes", joystick_get_axes},
    {"buttons", joystick_get_buttons},
    {0}
};

static struct StructInfo structJoystick = {"Joystick", structJoystickFields, joystick_copy, joystick_free};

static int joystick_open(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* dest) {
    struct DemoShell* shell = data;
    struct ShellRefCount* r;
    struct Joystick* joystick;
    char* path;
    const char* p;
    unsigned int i;
    int fd;
    char n;

    if (!nArgs) {
        path = NULL;
        p = "/dev/input/js0";
    } else if (nArgs == 1) {
        if (!(path = variable_to_string(args))) {
            fprintf(stderr, "Error: expected string argument\n");
            return 0;
        }
        p = path;
    } else {
        fprintf(stderr, "Error: expected one argument\n");
        return 0;
    }
    if ((fd = open(p, O_RDONLY | O_NONBLOCK)) == -1) {
        fprintf(stderr, "Error: failed to open %s\n", p);
        free(path);
        return 0;
    }
    free(path);
    if (!(joystick = malloc(sizeof(*joystick)))) {
        fprintf(stderr, "Error: failed to allocated joystick\n");
        close(fd);
        return 0;
    }
    for (i = 0; i < sizeof(joystick->axes) / sizeof(*joystick->axes); i++) {
        joystick->axes[i] = 0;
    }
    joystick->btns = 0;
    joystick->numBtns = 0;
    joystick->numAxes = 0;
    joystick->fd = fd;
    if (!(r = shell_new_refcount(shell, joystick_destroy, joystick))) {
        fprintf(stderr, "Error: failed to allocated joystick\n");
        joystick_destroy(shell, joystick);
        return 0;
    }
    if (!ioctl(fd, JSIOCGAXES, &n)) {
        if (n > sizeof(joystick->axes) / sizeof(*joystick->axes)) {
            n = sizeof(joystick->axes) / sizeof(*joystick->axes);
        }
        joystick->numAxes = n;
    }
    if (!ioctl(fd, JSIOCGBUTTONS, &n)) {
        joystick->numBtns = n;
    }
    variable_make_struct(&structJoystick, r, dest);
    return 1;
}

static int joystick_do_events(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* dest) {
    struct Variable vj, vc;
    struct js_event event;
    struct Joystick* joystick;
    struct Variable* callback = NULL;
    unsigned long n = 0;

    if (nArgs < 1 || nArgs > 2) {
        fprintf(stderr, "Error: joystick_do_events: expected 1 or 2 arguments\n");
        return 0;
    }
    if (!variable_copy_dereference(args, &vj)) {
        fprintf(stderr, "Error: joystick_do_events: failed to copy variable\n");
        return 0;
    }
    if (vj.type != STRUCT || vj.data.dstruct.info != &structJoystick) {
        fprintf(stderr, "Error: joystick_do_events: expected struct Joystick\n");
        variable_free(&vj);
        return 0;
    }
    joystick = ((struct ShellRefCount*)vj.data.dstruct.data)->data;
    if (nArgs == 2) {
        if (!variable_copy_dereference(args + 1, &vc)) {
            fprintf(stderr, "Error: joystick_do_events: failed to copy variable\n");
            variable_free(&vj);
            return 0;
        }
        callback = &vc;
        if (callback->type != FUNCTION) {
            fprintf(stderr, "Error: joystick_do_events: expected function\n");
            variable_free(&vj);
            variable_free(&vc);
            return 0;
        }
    }

    while (read(joystick->fd, &event, sizeof(event)) == sizeof(event)) {
        joystick_process_event(&event, joystick);
        if (!(event.type & JS_EVENT_INIT)) {
            if (callback) {
                float val = event.value;
                if (event.type == JS_EVENT_AXIS) val /= 32767.0f;
                variable_function_call_l(callback, dest, "%u%u%f%lu", (unsigned int)event.type, (unsigned int)event.number, val, (unsigned long)event.time);
                variable_free(dest);
            }
            n++;
        }
    }
    variable_make_int(n, dest);
    variable_free(&vj);
    if (callback) variable_free(callback);
    return 1;
}

int demo_plugin_init(struct DemoShell* shell, unsigned int apiver) {
    if (apiver != DEMO_API_VERSION) return 0;
    return builtin_function_with_data(shell, "joystick_open", joystick_open, shell)
        && builtin_function(shell, "joystick_do_events", joystick_do_events)
        && builtin_int(shell, "JOYSTICK_BUTTON", JS_EVENT_BUTTON)
        && builtin_int(shell, "JOYSTICK_AXIS", JS_EVENT_AXIS);
}
